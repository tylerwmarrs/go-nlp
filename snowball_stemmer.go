package nlp

import (
	"github.com/kljensen/snowball"
)

// Implements a word normalizer based on the
// Snowball Stemmer. Simply wraps the logic from
// https://github.com/kljensen/snowball
type SnowballStemmer struct{}

// Create new instance of SnowballStemmer
func NewSnowballStemmer() *SnowballStemmer {
	return new(SnowballStemmer)
}

// Implements the Normalize function for WordNormalizer.
// Currently this only makes use of english words, but could
// make use of other languages in the future as the library
// supports them.
func (stemmer *SnowballStemmer) Normalize(word string) string {
	stemmed, _ := snowball.Stem(word, "english", true)
	return stemmed
}
