package nlp

import (
	"regexp"
	"strings"
)

// Interface that provides a Word Tokenizer.
// It enables you to change how words are split up.
// By convention the Tokenize method should be an
// Iterator so it can be looped over.
type WordTokenizer interface {
	Tokenize(text string) <-chan *WordToken
}

// Container that provides the index that the word is found
// within the string while tokenizing some text.
type WordToken struct {
	// The position that this word was found in the text.
	// Note that positions start at 0!!!
	Index int
	// The word that was tokenized.
	Word string
}

// Helper to split words by space given a string.
func splitBySpace(text string) []string {
	wordRegex := regexp.MustCompile("\\w+")
	return wordRegex.FindAllString(text, -1)
}

// This is the default implementation of the Word
// Tokenizer. It simply splits words on the space.
type DefaultWordTokenizer struct{}

// Implements the Tokenize method for the DefaultWordTokenizer.
// It simply splits words by space.
func (tokenizer *DefaultWordTokenizer) Tokenize(text string) <-chan *WordToken {
	ch := make(chan *WordToken)
	go func() {
		defer close(ch)

		for i, w := range splitBySpace(text) {
			ch <- &WordToken{i, w}
		}
	}()
	return ch
}

// Convenience method to obtain a new DefaultWordTokenizer.
func NewDefaultWordTokenizer() *DefaultWordTokenizer {
	return &DefaultWordTokenizer{}
}

// Implementation of word tokenizer that skips provided stop words.
// Words are simply split by space.
type StopwordWordTokenizer struct {
	// Map that acts like a set for faster lookups.
	stopWords map[string]bool
}

// Implements the SetStopWords for the KeywordTextMiner.
// All incoming words are lowercased.
func (tokenizer *StopwordWordTokenizer) SetStopWords(words []string) {
	tokenizer.stopWords = make(map[string]bool)
	for _, w := range words {
		tokenizer.AddStopWord(w)
	}
}

// Implements the AddStopWord for the KeywordTextMiner.
// All incoming words are lowercased.
func (tokenizer *StopwordWordTokenizer) AddStopWord(word string) {
	if tokenizer.stopWords == nil {
		tokenizer.stopWords = make(map[string]bool)
	}

	word = strings.ToLower(word)
	word = strings.TrimSpace(word)
	tokenizer.stopWords[word] = true
}

// Implements the Tokenize method for the StopwordTokenizer.
// It splits words by space and skips provided stop words.
func (tokenizer *StopwordWordTokenizer) Tokenize(text string) <-chan *WordToken {
	ch := make(chan *WordToken)
	go func() {
		defer close(ch)

		for i, w := range splitBySpace(text) {
			if _, ok := tokenizer.stopWords[strings.ToLower(w)]; !ok {
				ch <- &WordToken{i, w}
			}
		}
	}()
	return ch
}

// Convenience method to obtain a new StopwordWordTokenizer.
func NewStopwordWordTokenizer() *StopwordWordTokenizer {
	return &StopwordWordTokenizer{}
}
