package nlp

import (
	"strings"
)

// The KeywordTextMiner is an implementation of TextMiner
// that only provides results for the given keywords.
type KeywordTextMiner struct {
	// The keywords to mine.
	// Using a map like a set here...
	Keywords map[string]bool
	// The word normalizer to use.
	normalizer WordNormalizer
	// The word tokenizer to use.
	tokenizer WordTokenizer
	// The stop words.
	// Using a map like a set here...
	stopWords map[string]bool
}

// Provides a KeywordTextMiner with the default word tokenizer.
func NewKeywordTextMiner() *KeywordTextMiner {
	miner := new(KeywordTextMiner)
	miner.SetWordTokenizer(NewDefaultWordTokenizer())

	return miner
}

// Implements the SetWordNormalizer for the KeywordTextMiner.
func (miner *KeywordTextMiner) SetWordNormalizer(normalizer WordNormalizer) {
	miner.normalizer = normalizer
}

// Implements the SetWordTokenizer for the KeywordTextMiner.
func (miner *KeywordTextMiner) SetWordTokenizer(tokenizer WordTokenizer) {
	miner.tokenizer = tokenizer
}

// Implements the SetStopWords for the KeywordTextMiner.
// All incoming words are lowercased.
func (miner *KeywordTextMiner) SetStopWords(words []string) {
	miner.stopWords = make(map[string]bool)
	for _, w := range words {
		miner.AddStopWord(w)
	}
}

// Implements the AddStopWord for the KeywordTextMiner.
// All incoming words are lowercased.
func (miner *KeywordTextMiner) AddStopWord(word string) {
	if miner.stopWords == nil {
		miner.stopWords = make(map[string]bool)
	}

	word = strings.ToLower(word)
	word = strings.TrimSpace(word)
	miner.stopWords[word] = true
}

// Sets keywords.
// All incoming words are lowercased and trimmed.
func (miner *KeywordTextMiner) SetKeywords(words []string) {
	miner.Keywords = make(map[string]bool)
	for _, w := range words {
		miner.AddKeyword(w)
	}
}

// Adds a keyword.
// All incoming words are lowercased and trimmed.
func (miner *KeywordTextMiner) AddKeyword(word string) {
	if miner.Keywords == nil {
		miner.Keywords = make(map[string]bool)
	}

	word = strings.ToLower(word)
	word = strings.TrimSpace(word)
	miner.Keywords[word] = true
}

// Implements the MineText for the KeywordTextMiner.
func (miner *KeywordTextMiner) MineText(text string) MinedResults {
	data := make(map[string]*MinedTextResult)

	for token := range miner.tokenizer.Tokenize(text) {
		normalWord := miner.normalizer.Normalize(token.Word)

		// check for stop word
		if _, ok := miner.stopWords[normalWord]; ok {
			continue
		}

		// check for keyword
		if _, ok := miner.Keywords[normalWord]; !ok {
			continue
		}

		// add text result or create it
		if _, ok := data[normalWord]; ok {
			data[normalWord].AddIndex(token.Index)
		} else {
			data[normalWord] = NewMinedTextResult(normalWord, token.Index)
		}
	}

	return data
}
