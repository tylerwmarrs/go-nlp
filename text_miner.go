package nlp

// Container to hold mined text results.
type MinedTextResult struct {
	// The text representing the mined result.
	Label string
	// Number of occurrences for this text.
	// Keep in mind it may be 0 depending on the
	// way you mined the data.
	Frequency int
	// The positions that the mined text was found
	// within the text. Positions start at 0 and
	// it may always be 0 depending on the mining
	// method chosen.
	Indices []int
}

// Creates a new MinedTextResult given label and index.
// The frequency is set to 1.
func NewMinedTextResult(l string, i int) *MinedTextResult {
	m := new(MinedTextResult)
	m.Label = l
	m.Frequency = 1
	m.Indices = make([]int, 0)
	m.Indices = append(m.Indices, i)
	return m
}

// Adds a new index to the MinedTextResult instance.
func (m *MinedTextResult) AddIndex(i int) {
	m.Indices = append(m.Indices, i)
	m.Frequency++
}

// Simplified type to represent results from mining text.
type MinedResults map[string]*MinedTextResult

// The TextMiner is an interface that provides standard
// methods that should be implemented.
// At a high level, it encapsulates the word normalizer,
// stop words, and convenience methods to mine words as
// indexed (every word found) or frequency (summary) results.
// Additionally, it enables you to swap out the WordTokenizer
// with different implementations.
type TextMiner interface {
	// Sets the word normalizer used for mining.
	SetWordNormalizer(normalizer WordNormalizer)
	// Sets the stop words to exclude from mined results.
	SetStopWords(words []string)
	// Adds a stop word
	AddStopWord(word string)
	// Mines the text providing positional information for each
	// word by index and the frequency. Note that the index starts at 0!
	MineText(text string) MinedResults
	// Sets the WordTokenizer to use.
	SetWordTokenizer(tokenizer WordTokenizer)
}
