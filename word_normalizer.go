package nlp

// The WordNormalizer interface enables an abstraction
// that when given a string it normalizes the word and
// returns the new result. This can be used for implementing
// either a stemmer or lemmatizer.
type WordNormalizer interface {
	// Normalizes the given word returning the new result.
	Normalize(word string) string
}
