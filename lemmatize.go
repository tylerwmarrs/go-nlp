package nlp

import (
	"strings"
)

// This is a specific way to normalize words. It enables
// users to create custom mappings of base words to
// synonyms. When provided a string it gives the word
// back in its base form or the original word back in return.
type Lemmatizer struct {
	Lemmas []Lemma
}

// Gets a new lemmatizer.
func NewLemmatizer() *Lemmatizer {
	l := new(Lemmatizer)
	l.Lemmas = make([]Lemma, 1)
	return l
}

// Adds a Lemma to the lemmatizer.
func (lmtzr *Lemmatizer) AddLemma(l *Lemma) {
	lmtzr.Lemmas = append(lmtzr.Lemmas, *l)
}

// Normalizes a word comparing against given lemmas or
// returns the original string when no matches are found.
func (lmtzr *Lemmatizer) Lemmatize(word string) string {
	var tmp string = word
	tmp = strings.ToLower(tmp)
	tmp = strings.TrimSpace(tmp)

	for _, lemma := range lmtzr.Lemmas {
		if lemma.Similar(word) {
			tmp = lemma.Word
			break
		}
	}

	return tmp
}

// Implements the WordNormalizer interface
func (lmtzr *Lemmatizer) Normalize(word string) string {
	return lmtzr.Lemmatize(word)
}

// Container for the lemma (base word) and equivalent words.
type Lemma struct {
	Word    string
	wordSet map[string]bool
}

// Creates a new Lemma with base word and equivalent words.
func NewLemma(word string, words []string) *Lemma {
	l := new(Lemma)
	l.Word = word
	l.wordSet = make(map[string]bool)

	for _, w := range words {
		l.wordSet[w] = true
	}

	return l
}

// Adds a new synonym to the lemma.
func (l *Lemma) Add(word string) {
	l.wordSet[word] = true
}

// Checks if the given string is equivalent to this lemma.
func (l *Lemma) Similar(word string) bool {
	_, found := l.wordSet[word]
	return found || l.Word == word
}
