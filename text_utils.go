package nlp

// Helper to get absolute distance between two word
// indices.
func AbsoluteDistance(a int, b int) int {
	distance := a - b
	if distance < 0 {
		return distance * -1
	}
	return distance
}

// Helper to see if a given word exists in the mined text.
func Contains(word string, minedText MinedResults) bool {
	_, ok := minedText[word]
	return ok
}

// Helper to see if any of the specified words are contained within the
// given map.
func ContainsAny(words []string, minedText MinedResults) bool {
	exists := false
	for _, w := range words {
		if _, ok := minedText[w]; ok {
			exists = true
			break
		}
	}
	return exists
}

// Helper to see if all of the specified words are contained within the
// given map.
func ContainsAll(words []string, minedText MinedResults) bool {
	exists := true
	for _, w := range words {
		if _, ok := minedText[w]; !ok {
			exists = false
			break
		}
	}
	return exists
}

// Helper to see if a given string comes before another string within the
// given map. We arbitrarily use a distance of 5 to check if word a is
// really before word b. This is due to us storing multiple indices for
// each occurrence of the word.
// WARNING: complexity is O(N2)
func IsBefore(a string, b string, minedText MinedResults) bool {
	distance := 5
	minedA, ok := minedText[a]
	if !ok {
		return false
	}
	minedB, ok := minedText[b]
	if !ok {
		return false
	}

	for _, indexA := range minedA.Indices {
		for _, indexB := range minedB.Indices {
			if AbsoluteDistance(indexB, indexA) <= distance {
				return true
			}
		}
	}

	return false
}

// Helper to see if a given string comes after another string within the
// given map. We arbitrarily use a distance of 5 to check if word a is
// really after word b. This is due to us storing multiple indices for
// each occurrence of the word.
// WARNING: complexity is O(N2)
func IsAfter(a string, b string, minedText MinedResults) bool {
	distance := 5
	minedA, ok := minedText[a]
	if !ok {
		return false
	}
	minedB, ok := minedText[b]
	if !ok {
		return false
	}

	for _, indexA := range minedA.Indices {
		for _, indexB := range minedB.Indices {
			if AbsoluteDistance(indexA, indexB) <= distance {
				return true
			}
		}
	}

	return false
}

// Helper to see if an array of strings appear in the given order inside the
// given map.
func AppearInOrder(words []string, minedText MinedResults) bool {
	meets := true
	for i := 0; i < len(words)-1; i++ {
		meets = meets && IsBefore(words[i], words[i+1], minedText)
	}
	return meets
}
